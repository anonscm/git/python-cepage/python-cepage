import Animal
import Zoo
import ZooDeplacement
import ZooFormat
import ZooUrl

print("Test de la classe Animal")
Animal.testAnimal()

print("\n\n**********************************\n\n")
print("\nTest de la classe Zoo")
Zoo.testZoo()

print("\n\n**********************************\n\n")
print("Test de la classe ZooDeplacement")
ZooDeplacement.testZooDeplacement()

print("\n\n**********************************\n\n")
print("Test de la classe ZooFormat")
ZooFormat.testZooFormat()

print("\n\n**********************************\n\n")
print("Test de la classe ZooUrl")
ZooUrl.testZooUrl()

