import re
import Animal
from Zoo import Zoo

class ZooDeplacement(Zoo):
    dictionnaire = {
        'girafe' : Animal.Terrien,
        'lion'   : Animal.Terrien,
        'python' : Animal.Terrien,
        'singe'  : Animal.Terrien,
        'souris' : Animal.Terrien,
        'aigle'  : Animal.Aerien,
        'grenouille' : Animal.Aquatique
    }

    def newAnimal(self, nom, genre, age, nourriture, arrivee, soigneur):
        classe = ZooDeplacement.dictionnaire[genre] if genre in ZooDeplacement.dictionnaire.keys() else Animal.Animal
        return classe(nom,genre,age,nourriture,arrivee,soigneur)

    def print(self, genre=None, soigneur=None, arrivee=None, deplacement=None):
        """
        Fonction d'impression des animaux.

        Paramètres optionnels:
        genre de l'animal : n'affiche que les animaux de ce genre
        date              : n'affiche que les animaux arrivés à cette date, la date peut être au format 'AAAA', 'MM/AAAA' ou 'DD/MM/AAAA'
        soigneur          : n'affiche que les animaux gérés par ce soigneur
        """
        # On trie la liste des animaux
        # ... soit en filtrant les animaux
        #liste = list(filter(lambda animal:animal if (deplacement == None or deplacement == type(animal)) else None, self.getAnimaux()))
        # ... soit en utilisant une liste de comprehension
        liste = [ animal for animal in self.getAnimaux() if deplacement == None or deplacement == type(animal)]
        Zoo.print_(self, liste, genre, soigneur, arrivee)

    def deplacement(self):
        aerien = len(list(filter(lambda e: isinstance(e, Animal.Aerien), self.getAnimaux())))
        aquatique = len(list(filter(lambda e: isinstance(e, Animal.Aquatique), self.getAnimaux())))
        terrien = len(list(filter(lambda e: isinstance(e, Animal.Terrien), self.getAnimaux())))
        autres = len(self.getAnimaux()) - aerien - aquatique - terrien

        aerien_text = "{} anima{} volant".format(aerien, "l" if aerien <= 1 else "ux");
        aquatique_text = "{} anima{} nageant".format(aquatique, "l" if aquatique <= 1 else "ux");
        terrien_text = "{} anima{} se déplacant sur terre".format(terrien, "l" if terrien <= 1 else "ux");
        autres_text = "" if autres == 0 else "et {} anima{} avec aucune information de déplacement".format(autres,
                                                                                                           "l" if autres == 1 else "ux")
        # ici on accede a un attribut privé de la classe Zoo en prefixant son nom par _Zoo
        print("Zoo de {} a {}, {}, {} {}".format(self._Zoo__nom,
                                                 aerien_text,
                                                 aquatique_text,
                                                 terrien_text,
                                                 autres_text))

def testZooDeplacement():
    print(ZooDeplacement.__doc__)
    pessac = ZooDeplacement("pessac", "pessac.csv")
    print("Tous les animaux...")
    pessac.print()

    print("")
    print("Tous les animaux qui volent...")
    pessac.print(deplacement=Animal.Aerien)

    print("")
    print("Tous les animaux qui volent arrivés en 2014...")
    pessac.print(deplacement=Animal.Aerien, arrivee="2014")

    print("")
    pessac.deplacement()

if __name__ == "__main__":
    testZooDeplacement()
