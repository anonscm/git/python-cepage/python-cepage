import re
import Animal
from ZooDeplacement import ZooDeplacement

class ZooFormat(ZooDeplacement):

    def lireLigne(self, animal):
        animal = re.sub('["\']', '', animal)
        infos = animal.rstrip().split(',')
        return infos + [None,None,None]

def testZooFormat():
    print(ZooFormat.__doc__)

    with open("vincennes.csv", "w") as fout:
        fout.write('"id","nom","genre","age"\n')
        fout.write('3,"Abigail","girafe",10\n')
        fout.write('1,"Tyrone","singe",2\n')

    vincennes = ZooFormat("vincennes", "vincennes.csv")
    print("Tous les animaux...")
    vincennes.print()

if __name__ == "__main__":
    testZooFormat()
