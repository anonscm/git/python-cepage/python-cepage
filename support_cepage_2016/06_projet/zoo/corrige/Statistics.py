import functools
import math
import doctest

def moyenne(valeurs):
    """
    calcule la moyenne d'une liste de nombres
    >>> moyenne([])
    >>> moyenne([12])
    12.0
    >>> moyenne([12,14,16,18])
    15.0
    """
    return None if len(valeurs) == 0 else functools.reduce(lambda a,b:a+b, valeurs) / len(valeurs)

def maximum(valeurs):
    """
    calcule le maximum d'une liste de nombres
    >>> maximum([])
    >>> maximum([12,78,7])
    78
    """
    return None if len(valeurs) == 0 else max(valeurs)

def ecart_type(valeurs):
    """
    calcule l'ecart type d'une liste de nombres
    >>> ecart_type([])
    >>> ecart_type([12,14,16,18])
    2.24
    """
    if len(valeurs) == 0:
        return None
    else:
        moy = moyenne(valeurs)
        somme = functools.reduce(lambda a,b:a+b,
                                 list(map(lambda x:(x-moy)**2, valeurs)))
        return round(math.sqrt(somme/len(valeurs)),2)

if __name__ == "__main__":
    doctest.testmod()
    print("La moyenne de {} est {}".format([12,14,16,18],moyenne([12,14,16,18])))
    print("Le max de {} est {}".format([12,14,16,18],maximum([12,14,16,18])))
    print("L'ecart type de {} est {}".format([12,14,16,18],ecart_type([12,14,16,18])))


