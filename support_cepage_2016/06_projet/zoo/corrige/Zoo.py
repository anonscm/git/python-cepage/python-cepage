import re
import Animal
import Statistics

class Zoo(object):
    """
    Initialisation de la classe Zoo.

    Paramètres:
    nom     : nom du Zoo
    fichier : Nom du fichier contenant la liste des animaux du zoo
    """

    def __init__(self, nom, fichier):
        self.__nom = nom
        self.__animaux = []

        animaux = self.lireFichier(fichier)
        for animal in animaux:
            infos = self.lireLigne(animal)
            self.__animaux.append(self.newAnimal(infos[1], infos[2], infos[3], infos[4], infos[5], infos[6]))

    def lireFichier(self, fichier):
        with open(fichier, "r") as fin:
            # on lit la 1ere ligne qui est l'entete
            entete = fin.readline()

            # on lit les animaux
            return fin.readlines()

    def lireLigne(self, animal):
        animal = re.sub('["\']', '', animal)
        infos = animal.rstrip().split(',')
        return infos

    def newAnimal(self, nom, genre, age, nourriture, arrivee, soigneur):
        return Animal.Animal(nom, genre, age, nourriture, arrivee, soigneur)

    def getAnimaux(self):
        return self.__animaux

    def print_(self, liste, genre=None, soigneur=None, arrivee=None):
        liste2 = list(filter(lambda animal:animal if (genre == None or genre == animal.getGenre()) else None, liste))
        liste3 = list(filter(lambda animal:animal if (soigneur == None or soigneur == animal.getSoigneur()) else None, liste2))
        liste4 = list(filter(lambda animal:animal if (arrivee == None or arrivee in animal.getDateArrivee()) else None, liste3))
        for animal in liste4:
            print(animal)

    def print(self, genre=None, soigneur=None, arrivee=None):
        """
        Fonction d'impression des animaux.

        Paramètres optionnels:
        genre de l'animal : n'affiche que les animaux de ce genre
        date              : n'affiche que les animaux arrivés à cette date, la date peut être au format 'AAAA', 'MM/AAAA' ou 'DD/MM/AAAA'
        soigneur          : n'affiche que les animaux gérés par ce soigneur
        """
        print("Zoo de {}".format(self.__nom))
        self.print_(self.__animaux, genre, soigneur, arrivee)

    def get_statistiques_age(self, genre=None):
        liste = list(filter(lambda animal:animal if (genre == None or genre == animal.getGenre()) else None, self.__animaux))
        ages=list(map(lambda animal:float(animal.getAge()),liste))
        return Statistics.moyenne(ages),Statistics.maximum(ages),Statistics.ecart_type(ages)

    def print_statistiques_age(self, message="", genre=None):
        moyenne,maximum,ecart_type = self.get_statistiques_age(genre)
        print("[{}] moyenne={} , maximum={}, ecart type={}".format(message, moyenne, maximum, ecart_type))

def testZoo():
    print(Zoo.__doc__)

    with open("pessac.csv", "w") as fout:
        fout.write('"id","nom","genre","age","nourriture","date_arrivee","soigneur"\n')
        fout.write('3,"Abigail","girafe",10,"Feuille accacia",11/11/2012,"Nathalie"\n')
        fout.write('1,"Tyrone","singe",2,"banane",07/01/2012,"Fabrice"\n')
        fout.write('40,"Lareina","singe",3,"banane",25/12/2016,"Sandrine"\n')
        fout.write('37,"Sheila","zebre",5,"blé",03/09/2016,"Nathalie"\n')
        fout.write('1,"Tyrone","grenouille",2,"insecte",07/01/2012,"Fabrice"\n')
        fout.write('32,"Abdul","aigle",1,"blé",20/03/2016,"Nathalie"\n')
        fout.write('10,"Eden","aigle",7,"blé",13/03/2014,"Nathalie"\n')
    pessac = Zoo("pessac", "pessac.csv")

    print("Tous les animaux...")
    pessac.print()

    print("\nSeulement les singes...")
    pessac.print(genre="singe")

    print("\nSeulement les singes nourri par Fabrice...")
    pessac.print(genre="singe", soigneur="Fabrice")

    print("\nSeulement les singes nourri par Hubert...")
    pessac.print(genre="singe", soigneur="Hubert")

    print("\nSeulement les animaux arrivée en 2012...")
    pessac.print(arrivee="2012")

    print("")
    pessac.print_statistiques_age("Statistiques d'âge pour tous les animaux")
    pessac.print_statistiques_age("      Statistiques d'âge pour les singes", genre="singe")

if __name__ == "__main__":
    testZoo()
