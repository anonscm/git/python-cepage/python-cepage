class Animal(object):
    """
    Initialisation de la classe Animal.

    Paramètres:
    nom          : nom de l'animal
    genre        : genre de l'animal
    age          : age de l'animal
    nourriture   : ce que mange l'animal
    date_arrivee : date d'arrivée de l'animal au zoo au format JJ/MM/AAAA
    soigneur     : soigneur responsable de l'animal
    """

    def __init__(self,nom,genre,age,nourriture,arrivee,soigneur):
        self.__nom = nom
        self.__genre = genre
        self.__age = age
        self.__nourriture = nourriture
        self.__arrivee = arrivee
        self.__soigneur = soigneur

    def __str__(self):
        return "{} ({}) âgé de {} ans, arrivé le {}, soigné par {} et nourri avec {}".format(self.__nom, self.__genre,
                                                                                             self.__age, self.__arrivee,
                                                                                             self.__soigneur,
                                                                                             self.__nourriture)

    def getGenre(self):
        return self.__genre

    def getSoigneur(self):
        return self.__soigneur

    def getDateArrivee(self):
        return self.__arrivee

    def getAge(self):
        return self.__age

    def getNom(self):
        return self.__nom

    def deplacement(self):
        print("aucune information sur le deplacement de {} ({})".format(self.__nom, self.__genre))

class Aerien(Animal):
    def deplacement(self):
        print("{} ({}) believes he can fly".format(self.getNom(), self.getGenre()))

class Aquatique(Animal):
    def deplacement(self):
        print("{} ({}) est aquatique".format(self.getNom(), self.getGenre()))

class Terrien(Animal):
    def deplacement(self):
        print("{} ({}) se déplace sur ses pattes".format(self.getNom(), self.getGenre()))

def testAnimal():
    print(Animal.__doc__)
    titi = Animal("titi", "canari", 12, "graines", "12/12/2008", "grosminet")
    print(titi)
    titi.deplacement()
    print()

    tyrone = Aquatique("Tyrone","grenouille",2,"insecte","07/01/2012","Fabrice")
    print(tyrone)
    tyrone.deplacement()
    print()

    abdul = Aerien("Abdul","aigle",1,"blé","20/03/2016","Nathalie")
    print(abdul)
    abdul.deplacement()
    print()

    lareina = Terrien("Lareina","singe",3,"banane","25/12/2016","Sandrine")
    print(lareina)
    lareina.deplacement()
    print()

if __name__ == "__main__":
    testAnimal()

