import unittest
import Statistics

class TestStatistics(unittest.TestCase):
    def test_moyenne(self):
        self.assertEqual(Statistics.moyenne([12,14,16]), 14.0)

    def test_moyenne_1(self):
        self.assertEqual(Statistics.moyenne([]), None)

    def test_moyenne_2(self):
        self.assertEqual(Statistics.moyenne([12]), 12.0)

    def tearDown(self):
        print("ohohoh")

    def test_ecart_type_0(self):
        self.assertEqual(Statistics.ecart_type([]), None)

    def test_ecart_type_1(self):
        self.assertEqual(Statistics.ecart_type([12,14,16,18]), 2.24)

    def test_maximum_0(self):
        self.assertEqual(Statistics.maximum([]), None)

    def test_maximum_1(self):
        self.assertEqual(Statistics.maximum([12,78,7]), 78)

if __name__ == '__main__':
    print("Testing")
    unittest.main(verbosity=2)

