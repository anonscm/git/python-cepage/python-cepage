from Zoo import Zoo
import urllib.request
import zipfile

class ZooUrl(Zoo):
    """
    Initialisation de la classe ZooUrl.

    Paramètres:
    nom   : nom du Zoo
    url   : URL pointant sur un fichier contenant la liste des animaux du zoo
    """

    def lireFichier(self, url):
        outfile = "/tmp/foo"
        # On recupere ce que pointe l'url dans un fichier local, le
        # fichier recupere est au format binaire et contient une
        # chaine, on ecrit cette chaine dans un fichier temporaire
        with urllib.request.urlopen(url) as r:
            with open(outfile, 'wb') as w:
                buffer = r.read()
                w.write(buffer)

        # on essaye de unzipper le fichier
        zfile = None
        try:
            zfile =  zipfile.ZipFile(outfile, "r")
        except Exception:
            pass # le fichier n'est pas une archive Zip

        if (zfile != None):
            # On suppose que le zip ne contient qu'un seul fichier
            zfile.extract(zfile.namelist()[0], "/tmp")
            outfile = "/tmp/" + zfile.namelist()[0]

        # on appelle la fonction de la classe mere pour lire le fichier obtenu
        return Zoo.lireFichier(self, outfile)

def testZooUrl():
    print(ZooUrl.__doc__)

    gruss = ZooUrl("gruss", "http://localhost:8888/notebooks/06_projet/zoo/data/python_zoo_data.csv")
    print("Tous les animaux...")
    gruss.print()

    gruss2 = ZooUrl("gruss", "http://localhost:8888/notebooks/06_projet/zoo/data/python_zoo_data.csv.zip")
    print("Tous les animaux...")
    gruss2.print()

if __name__ == "__main__":
    testZooUrl()
