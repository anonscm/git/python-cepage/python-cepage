#!/usr/bin/env python3
#-*- coding: utf-8 -*-
##############################################################################
# $Id$
# test_wget.py crée avec cs.py par mendes le '2016-01-25 16:29:47'
# VERSION=0.0.1
# Objectif : 
#  
#  Premiers pas pour le TP et faire télécharger puis ouvrir le fichier zip
#
#
# Author: Fabrice Mendes
# Last Revision :
# - $Revision$
# - $Author$
# - $Date$
#
######################################################(FAb)###################


#import
import urllib.request
import zipfile

url_dl = 'http://localhost:8888/zoo.zip'
url_dl = 'http://localhost/data_tp.zip'
outfile = 'dl.zip'

def dl_file(url, outfilename):
    """Télécharge un fichier au bout de l'url """
    with urllib.request.urlopen(url_dl) as r:
        with open(outfilename, 'wb') as w:
            buffer = r.read()
            w.write(buffer)

def hello(a, b=2, c=3):
    """Test des paramètres optionnels et du _   """
    print("a=", "b=", b, "c=", c)


###
### MAIN
### 
if __name__ == '__main__':
    try:
        dl_file(url_dl, outfile)
    except Exception as e:
        print("Aïe sur le DL")
        exit(1)

    #hello(1, _, 3.1415)

    try:
        zfile =  zipfile.ZipFile(outfile, "r")
    except Exception as E:
        print("le fichier {} n'est pas une archive Zip.\n".format(outfile), E)
        exit(2)

    # list filenames
    print("## Contenu de l'archive")

    for name in zfile.namelist():
        zfile.extract(name, 'out_zip')
        print("extracting.... ", name)

    # list file information
    print("## Information sur les fichiers  de l'archive")
    for info in zfile.infolist():
        print(info.filename, info.date_time, info.file_size)
        #zfile.extract(name, '')

        
    print("Job done")
