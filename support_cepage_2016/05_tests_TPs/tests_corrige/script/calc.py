#!/usr/bin/env python

from calculatrice.calculatrice import Calculatrice
import sys

if len(sys.argv) != 2:
   print("""
usage
=====

./calc.py s

where s is the expression to compute as a string
""")
   sys.exit()

c = Calculatrice(sys.argv[1])
c.calcul()
