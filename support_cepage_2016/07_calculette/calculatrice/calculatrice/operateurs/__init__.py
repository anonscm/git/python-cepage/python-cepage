print("Chargement des opérations")

# Cela fonctionne mais pourquoi ? : à cause de sys.path
#import calculatrice.operateurs.addition
#import calculatrice.operateurs.soustraction
#import calculatrice.operateurs.multiplication
#import calculatrice.operateurs.division

# Hmmm, permet le chargement automagique
#__all__ = ['addition','soustraction', 'multiplication', 'division']

# Version python2 :
#import addition
#import soustraction
#import multiplication
#import division


# Chargement relatif à __init__.py (>= python 2.5)
from . import addition
from . import soustraction
from . import multiplication
from . import division

def identity(a,b):
    return b

operations = {
    '+': addition.addition,
    '-': soustraction.soustraction,
    '*': multiplication.multiplication,
    '/': division.division,
    '=': identity
}

