# -*- coding: utf-8 -*-

import calculatrice.analyseur
import calculatrice.operateurs

def calcule(chaine):
    res=0
    opList, numList = calculatrice.analyseur.analyse(chaine)
    print("( {} {} )".format(opList,numList))
    for op, num in zip(opList, numList):
        # on recupere la fonction associee a l'operateur
        fct = calculatrice.operateurs.operations[op]
        res = fct(res,num)
    return(res)

if __name__ == '__main__':
    res=calcule("7*6-12/2")
    print("resultat de {}={}".format("7*6-12/2", res))

