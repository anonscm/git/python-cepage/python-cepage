
import operateurs.addition
import operateurs.soustraction
import operateurs.multiplication
import operateurs.division

operations = {
    '+': addition.addition,
    '-': soustraction.soustraction,
    '*': multiplication.multiplication,
    '/': division.division
}

