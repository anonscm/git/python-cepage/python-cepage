#!/usr/bin/env python
# coding=UTF-8

def mul(a, b):
	pass

def div(a, b):
	pass

def add(a, b):
	pass

def sub(a, b):
	pass

def divInfinityOnZeroTest(a, b):
	# utiliser la fonction float("inf") (-float("inf")) pour représenter +∞ et -∞ ;
	# si b est égal à 0, retourner +∞ ou -∞.
	return div(a, b)
def divInfinityOnZeroException(a, b):
	# utiliser la fonction float("inf") (-float("inf")) pour représenter +∞ et -∞ ;
	# exécuter la division sans conditions ;
	# récupérer l'exception ZeroDivisionError et retourner alors +∞ ou -∞.
	return div(a, b)

print "Exercice 2.1"
print '  1 * 3 = ', mul(1, 3)
print "  1 / 3 = ", div(1, 3)
print "  1 + 3 = ", add(1, 3)
print "  1 - 3 = ", sub(1, 3)
print "  (1 + 3) * 11 + 1 - 3 = ", add(mul(add(1, 3), 11), sub(1, 3))

print "Exercice 2.2"
print "  +1 / 0 = ", divInfinityOnZeroTest(+1, 0)
print "  -1 / 0 = ", divInfinityOnZeroTest(-1, 0)
print "  +1 / 0 = ", divInfinityOnZeroException(+1, 0)
print "  -1 / 0 = ", divInfinityOnZeroException(-1, 0)
