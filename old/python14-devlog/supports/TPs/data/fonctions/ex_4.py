# coding=UTF-8
import codecs

# Dictionnaire de conversion de devises
DEVISES = {
	u'€':{ u'€':1.00, u'$':1.25, u'£':0.80 },
	u'$':{ u'€':0.80, u'$':1.00, u'£':0.64 },
	u'£':{ u'€':1.25, u'$':1.56, u'£':1.00 }
}

def conversionDeDevise(valeur, deviseValeur, deviseRetour):
	"""Convertit la "valeur" exprimée en "deviseValeur" vers la devise "deviseRetour", arrondi a deux chiffres après la virgule.
	
	Args :
		valeur(float)     -- la valeur à convertir ;
		deviseValeur(str) -- la devise de la valeur à convertir ;
		deviseRetour(str) -- la devise attendue par l'appeleur de cette fonction.
    Returns :
		float : la "valeur" exprimée en "deviseRetour"
	"""
	return round(DEVISES[deviseValeur][deviseRetour] * valeur, 2)

def lectureLigne(ligne):
	"""Lit une ligne du fichier csv sous forme d'un tuple (Libellé,Auteur,Prix,Devise)
	
	Args :
		ligne(str) -- une ligne du fichier csv
    Returns :
		tuple : un tuple (Libellé:str,Auteur:str,Prix:float,Devise:str)
	"""
	# indices:
	#	- dans ipython taper help(str) et regarder la documentation de la méthode "split" ;
	#	- dans ipython taper help(str) et regarder la documentation de la méthode "strip".
	return ligne.split(',')[0].strip(), ligne.split(',')[1].strip(), float(ligne.split(',')[2].strip()), ligne.split(',')[3].strip()
def lectureCSV(chemin, avecEntete=False):
	"""Lit le fichier csv qui se trouve dans "chemin" et le convertit en une liste de tuples (Libellé,Auteur,Prix,Devise).
	
	Args:
		chemin(str) -- le chemin vers le fichier csv.
    Returns:
		list: une liste des tuples (Libellé,Auteur,Prix,Devise) contenus dans le fichier csv, cette liste n'inclut pas l'entête.
	"""
	f = codecs.open(chemin, encoding='utf-8', mode='r')
	resultat = []
	if avecEntete:
		f.readline()
	# lecture du fichier ligne par ligne
	for ligne in f:
		resultat.append(lectureLigne(ligne))
	return resultat

def conversionDonnees(donnees, deviseRetour):
	"""Convertit une liste de tuples (Libellé,Auteur,Prix,Devise) en liste de tuples (Libellé,Auteur,Prix,deviseRetour).
	
	Args :
		donnees(list)     -- une liste des tuples (Libellé,Auteur,Prix,Devise) ;
		deviseRetour(str) -- la devise attendue pour toutes les cases de la liste de retour.
    Returns :
		list : une liste des tuples (Libellé,Auteur,Prix,deviseRetour).
	"""
	for index in range(len(donnees)):
		bouquin = donnees[index]
		donnees[index] = bouquin[0], bouquin[1], conversionDeDevise(bouquin[2], bouquin[3], deviseRetour), deviseRetour
	return donnees

def ecritureCSV(chemin, donnees):
	"""Crée un fichier csv dans "chemin" (et l'écrase s'il existe déjà) et écrit les "donnees" au format csv.
	
	Args :
		chemin(str)   -- le chemin vers le nouveau fichier csv ;
		donnees(list) -- une liste des tuples (Libellé,Auteur,Prix,Devise).
	"""
	f = codecs.open(chemin, encoding='utf-8', mode='w+')
	for bouquin in donnees:
		f.write(bouquin[0] + "," + bouquin[1] + "," + str(bouquin[2]) + "," + bouquin[3] + "\n")
	f.close()

