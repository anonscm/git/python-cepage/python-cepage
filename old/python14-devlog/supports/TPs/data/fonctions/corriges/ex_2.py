# coding=UTF-8

def mul(a, b):
	return a * b

def div(a, b):
	return a / b

def add(a, b):
	return a + b

def sub(a, b):
	return a - b

def divInfinityOnZeroTest(a, b):
	if b == 0:
		return float("inf") if a > 0 else -float("inf")
	return div(a, b)
def divInfinityOnZeroException(a, b):
	try:
		return div(a, b)
	except ZeroDivisionError:
		return float("inf") if a > 0 else -float("inf")
