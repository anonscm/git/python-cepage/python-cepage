# coding=UTF-8
import random, math

LIST_DATA = [ 4.1, 9, 0, "coucou", -1000, 1000, [ 0, 1], "42", -float("inf"), 18, -49, -4999.0, -455, float("inf"), -541, 9999.9, float("nan") ]
LIST_DATA.extend(4.2 * random.randint(-1000,1000) for r in xrange(20))
CHAINE = "abcd0ef9ghij8klmA5BCD7EFG6HIJK4LMNO3PQR2STU1VWXYZnopqrstuvwxyz"

# Exercie 3.1: Filtrage de données de type liste
# ecrire un filtre de type lamda qui selection les données de types entières et flotante
LIST_DATA1 = filter(lambda e: isinstance(e, int) or isinstance(e, float), LIST_DATA)

# Exercie 3.2: Filtrage de données de type liste
# ecrire un filtre de type lamda qui selection les données comprise entre [-1000 et 1000[
LIST_DATA2 = filter(lambda e: -1000 <= e < 1000, LIST_DATA1)

# Exercie 3.3: "Mappage" de données de type liste
# ecrire un map de type lamda qui transforme données en valeur absolues
LIST_DATA3 = map(lambda e: e * -1. if e < 0. else e, LIST_DATA2)
	
# Exercie 3.4: "Réduction" de données de type liste
# ecrire un reduce de type lamda qui calcule la multiplication des données tronquées en entier (0 doit être traité comme 1)
LIST_DATA4 = reduce(lambda x, y: 1 if (int(x) == 0) else int(x) * 1 if (int(y) == 0) else int(y), LIST_DATA3)

# Exercie 3.5: Filtrage de données de type chaine
# ecrire un filtre de type lamda qui selection les characters entre e et n et également entre E et N
CHAINE5 = filter(lambda e: ord('e')<=ord(e)<=ord('n') or ord('E')<=ord(e)<=ord('N'), CHAINE)
