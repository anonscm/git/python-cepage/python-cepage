#!/usr/bin/env python
# coding=UTF-8

from ex_4 import *

# Exercice 4.1: Conversion de devises
#	- Compléter la fonction conversionDeDevise grâce au dictionnaire "DEVISES".
# Vérification (non exhaustive) des spécifications de la fonction conversionDeDevise.
print "\nVérifications exercice 4.1"
try:
	erreurDetecte = False
	if conversionDeDevise(1., u"€", u"€") != 1.:
		print " - Erreur de conversion € => €"
		erreurDetecte = True
	if conversionDeDevise(1., u"€", u"$") != 1.25:
		print " - Erreur de conversion € => $"
		erreurDetecte = True
	if conversionDeDevise(1., u"£", u"$") != 1.56:
		print " - Erreur de conversion £ => $"
		erreurDetecte = True
	if conversionDeDevise(1., u"$", u"€") != .8:
		print " - Erreur de conversion $ => €"
		erreurDetecte = True

	if erreurDetecte:
		print "Vous devez corriger vos erreurs."
	else:
		print "Bravo, la conversion semble fonctionner correctement."
except NameError:
	print "Vous devez implémenter la fonction conversionDeDevise"



# Exercice 4.2: Transformation d'un texte vers une structure de données.
#	- Compléter la fonction lectureLigne.
# Vérification (non exhaustive) des spécifications de la fonction lectureLigne.
print "\nVérifications exercice 4.2"
try:
	erreurDetecte = False
	attendu = u'abcd', u'efg h', 42.0, u'$'
	obtenu  = lectureLigne(u"abcd,efg h,42.0,$")
	if obtenu != attendu or not isinstance(obtenu[2], float):
		print "erreur, obtenu:", obtenu, ", attendu:", attendu
		erreurDetecte = True
	attendu = u'abcd', u'efg h', 4.2, u'€'
	obtenu  = lectureLigne(u"abcd  ,efg h, 4.2, €")
	if obtenu != attendu or not isinstance(obtenu[2], float):
		print "erreur, obtenu:", obtenu, ", attendu:", attendu
		erreurDetecte = True

	if erreurDetecte:
		print "Vous devez corriger vos erreurs."
	else:
		print "Bravo, la conversion d'une ligne semble fonctionner correctement."
except NameError:
	print "Vous devez implémenter la fonction lectureLigne"


# Exercice 4.3: Lecture d'un fichier.
#	- Compléter la fonction lectureCSV
# Vérification (non exhaustive) des spécifications de la fonction lectureCSV.
print "\nVérifications exercice 4.3"
try:
	donnees = lectureCSV('data_3_v0.csv')
	attendu = u'Python Pocket Reference 5ed', u'Mark Lutz', 12.62, u'€'
	if donnees[1] == attendu:
		print "Bravo, la lecture de fichier semble fonctionner correctement."
	else:
		print "Vous devez corriger vos erreurs."
except NameError:
	print "Vous devez implémenter la fonction lectureCSV"

# Exercice 4.4: Gestion de l'entête.
#	- Modifier la fonction lectureCSV de telle façon qu'elle ignore la ligne 
#   d'entête lorsque la variable avecEntete est True.
# Vérification (non exhaustive) des spécifications de la fonction lectureCSV.
print "\nVérifications exercice 4.4"
try:
	erreurDetecte = True
	attendu = u'Python Pocket Reference 5ed', u'Mark Lutz', 12.62, u'€'
	try:
		donnees = lectureCSV('data_3_v1.csv', True)
		if donnees[1] == attendu:
			erreurDetecte = False
	except ValueError:
		pass
	if erreurDetecte:
		print "Vous devez corriger vos erreurs."
	else:
		print "Bravo, la lecture de fichier avec entête semble fonctionner correctement."
except NameError:
	print "Vous devez implémenter la fonction lectureCSV"


# Exercice 4.5: Conversion en €
#	- Compléter la fonction conversionDonnees en modifiant les données pour que toutes les lignes soient en €.
print "\nVérifications exercice 4.5"
try:
	erreurDetecte = True
	donnees2 = conversionDonnees(donnees, u'€')
	attendu = u'Python 3', u'Sébastien CHAZALLET', 38.89, u'€'
	if donnees2[2] == attendu:
		print "Bravo, la conversion des données semble fonctionner correctement."
		if donnees2 is donnees:
			erreurDetecte = False
		else:
			print " ! Cependant la modification doit être faite en place."
	if erreurDetecte:
		print "Vous devez corriger vos erreurs."
except NameError:
	print "Vous devez implémenter la fonction conversionDonnees"


# Exercie 4.6: Écriture d'un fichier
#	- Compléter la fonction ecritureCSV
print "\nVérifications exercice 4.6"
try:
	ecritureCSV('data_out.csv', donnees)
	donneesTest = lectureCSV('data_out.csv')
	if donnees == donneesTest:
		print "Bravo, l'écriture de fichier semble fonctionner correctement."
	else:
		print "Vous devez corriger vos erreurs."
except NameError:
	print "Vous devez implémenter la fonction ecritureCSV et la fonction lectureCSV"

