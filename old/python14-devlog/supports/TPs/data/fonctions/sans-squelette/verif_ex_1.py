#!/usr/bin/env python
# coding=UTF-8

from ex_1 import *


print "**Le résultat attendu est :\nBonjour"
print "->Votre résultat :"
try:
	afficheBonjour()
except NameError:
	print "La fonction afficheBonjour() n'est pas définie"

print "\n**Le résultat attendu est :\nBonjour tata TODO"
print "->Votre résultat :"
try:
	afficheBonjourA("TODO", "tata")
except NameError:
	print "La fonction afficheBonjourA() n'est pas définie"


