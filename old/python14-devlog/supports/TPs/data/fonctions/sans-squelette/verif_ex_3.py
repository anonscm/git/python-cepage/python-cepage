#!/usr/bin/env python
# coding=UTF-8
from ex_3 import *








print "\nVérifications exercice 3.1"
erreurDetecte = False
for e in LIST_DATA1:
	if not (isinstance(e, int) or isinstance(e, float)):
		print "  - ", e, "n'est ni entier ni flotant"
		erreurDetecte = True
if erreurDetecte:
	print "Vous devez corriger vos erreurs."
else:
	print "Bravo, le filtre semble fonctionner correctement."

print "\nVérifications exercice 3.2"
erreurDetecte = False
for e in LIST_DATA2:
	if isinstance(e, int) or isinstance(e, float):
		if math.isnan(e) or e >= 1000 or e < -1000:
			print "  - ", e, "n'est pas dans l'interval [-1000..1000["
			erreurDetecte = True
if erreurDetecte:
	print "Vous devez corriger vos erreurs."
else:
	print "Bravo, le filtre semble fonctionner correctement."

print "\nVérifications exercice 3.3"
erreurDetecte = False
for e in LIST_DATA3:
	if isinstance(e, int) or isinstance(e, float):
		if (not math.isnan(e)) and (-1000<e<=1000):
			if e < 0:
				print "  - ", e, "n'est pas >= 0"
				erreurDetecte = True
if erreurDetecte:
	print "Vous devez corriger vos erreurs."
else:
	print "Bravo, le map semble fonctionner correctement."

print "\nVérifications exercice 3.4"
verif = 1.
for e in LIST_DATA3:
	if (isinstance(e, int) or isinstance(e, float)) and (not math.isnan(e)) and (0<=e<=1000):
		e = int(e)
		verif = verif * 1 if (e == 0) else e
if LIST_DATA4 != verif:
	print "Vous devez corriger vos erreurs."
else:
	print "Bravo, la réduction semble fonctionner correctement."

print "\nVérifications exercice 3.5"
if CHAINE5 == 'efghijklmEFGHIJKLMNn':
	print "Bravo, le filtre semble fonctionner correctement."
else:
	print "Vous devez corriger vos erreurs."

