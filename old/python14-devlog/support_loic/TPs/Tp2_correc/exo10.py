#!/usr/bin/env python

Chaine = "Ceci est une chaine"

print dir(Chaine)

CHAINE = Chaine.upper()

print CHAINE

nb_mot = len(Chaine.split())

chaine = Chaine.lower()
# on enleve tous les espaces et le point
chaine = chaine.replace(' ', '').replace('.', '')

lettres = {}

for c in chaine:
    lettres[c] = lettres.get(c, 0) + 1

print lettres
