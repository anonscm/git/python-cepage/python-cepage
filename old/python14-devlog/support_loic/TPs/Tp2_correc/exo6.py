#/usr/bin/env python

A = [1, 2, 3, 4, 5]

B = [0]*len(A)

for i in range(len(A)):
    B[i] = A[i]

#affichage de l'adresse et du contenu des listes
print id(A), A 
print id(B), B
