#!/usr/bin/env python

for i in range(7, 21*7, 7):
    if i%3 == 0:
        print str(i) + '*',
    else:
        print i,
