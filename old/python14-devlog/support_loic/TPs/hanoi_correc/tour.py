

class Tour:
    def __init__(self, hauteur_max):
        self.hauteur_max = hauteur_max
        self.disques = []

    def remplir(self):
        self.disques = range(1, self.hauteur_max + 1)
        self.disques.reverse()

    def sommet(self):
        if self.disques == []:
            return 0
        return self.disques[-1]

    def disque(self, etage):
        if etage > len(self.disques):
            return 0
        return self.disques[etage - 1]

    def nbDisques(self):
        return len(self.disques)

    def transferer(self, tour):
        if self.nbDisques() == 0:
            print "il n'y a pas de disques a deplacer !"
            return
        
        if tour.nbDisques() == 0 or tour.sommet()>self.sommet():
            tour.disques.append(self.sommet())
            self.disques.pop()
        else:
            print "impossible de deplacer"
            return


