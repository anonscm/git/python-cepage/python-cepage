import string
import os, time
from hanoi import Hanoi

class HanoiTxt:
    def __init__(self, hauteur):
        self.hauteur = hauteur
        self.hanoi = Hanoi(hauteur)

    def reprDisque(self, n):
        if n == 0:
            return '!'
        return '<' + '='*n + '!' + '='*n + '>'

    def afficheJeu(self):
        os.system("clear")
        width = self.hauteur*2 + 4 + 1

        print (string.center('o', width) + ' ')*3

        for h in xrange(self.hauteur,0,-1):
            for t in self.hanoi.tours:
                print string.center(self.reprDisque(t.disque(h)), width),
            print
            
        print (string.center('~'*width, width) + ' ')*3

    def demandeTourDepart(self):
        debTour = -1
        while not self.hanoi.departValide(debTour):
            debTour = int(raw_input("Entrez la tour de depart -> "))
        return debTour

    def demandeTourArrivee(self, debTour):
        finTour = -1
        while not self.hanoi.arriveeValide(debTour, finTour):
            finTour = int(raw_input("Entrez la tour d'arrivee -> "))
        return finTour

    def jeuEtape(self):
        self.afficheJeu()
        debTour = self.demandeTourDepart()
        finTour = self.demandeTourArrivee(debTour)
        self.hanoi.tours[debTour].transferer(self.hanoi.tours[finTour])

    def jeu(self):
        while not self.hanoi.fini():
            self.jeuEtape()
        self.afficheJeu()

    def automatiqueGame(self, n, a, b ,c):
        if n == 1:
            self.hanoi.tours[a].transferer(self.hanoi.tours[c])
            self.afficheJeu()
            time.sleep(.1)
        else:
            self.automatiqueGame(n-1, a, c , b)
            self.hanoi.tours[a].transferer(self.hanoi.tours[c])
            self.afficheJeu()  
            time.sleep(.1)
            self.automatiqueGame(n-1, b, a , c)

