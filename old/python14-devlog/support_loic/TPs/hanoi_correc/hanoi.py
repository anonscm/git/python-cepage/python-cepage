from tour import Tour

class Hanoi:
    def __init__(self, hauteur_max):
        self.hauteur_max = hauteur_max
        self.tours = [Tour(hauteur_max),
                      Tour(hauteur_max),
                      Tour(hauteur_max)]
        self.tours[0].remplir()

    def departValide(self, numTour):
        if numTour<0 or numTour>2:
            return False
        if self.tours[numTour].nbDisques() == 0:
            return False

        return True

    def arriveeValide(self, debTour, finTour):
        if debTour == finTour:
            return False

        if self.departValide(debTour) == False:
            return False

        if finTour<0 or finTour>2:
            return False

        if self.tours[debTour].sommet()>self.tours[finTour].sommet() and self.tours[finTour].sommet()!=0:
            return False

        return True

    def fini(self):
        if self.tours[2].nbDisques() == self.hauteur_max:
            return True
        return False

        

