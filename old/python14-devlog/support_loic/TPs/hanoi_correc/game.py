from hanoi_txt import HanoiTxt

try:
    n = int(raw_input('Combien voulez-vous de disques ?\n'))
    jeu = HanoiTxt(n)
    
    s = ''
    while s not in ['o', 'n']:
        s = raw_input("voulez-vous jouer ? (o:oui, n:non) ")
        if s == 'o':
            jeu.jeu()
        if s == 'n':
            jeu.automatiqueGame(n, 0, 1, 2)
except ValueError:
    print "Le nombre de disques doit etre un entier !"
