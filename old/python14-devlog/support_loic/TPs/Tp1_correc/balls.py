#!/usr/bin/env python
# Exemple utilisation du Canvas Tk 
from Tkinter import *
from random import randint, choice
import time

WIDTH, HEIGHT = 500, 400 # canvas size
X_DIAM = 20              # diametre de la balle
Y_DIAM = X_DIAM
DELAY = 0.01

class Balle:
    def __init__(self, canvas, x, y, vx=1, vy=1, color='green'):
        """
        Initialisation de la classe Balle

        Entrees:
        --------
        
            canvas : fenetre graphique ou la balle doit etre affichee 
            x : coordonnee de la balle suivant x
            y : coordonnee de la balle suivant y
            vx : vitesse de la balle suivant x
            vy : vitesse de la balle suivant y
            color : couleur de la balle

        """ 
        self.canvas = canvas
        self.x, self.y = x, y
        self.vx, self.vy = vx, vy
        self.color = color
        self.balle = self.canvas.create_oval(self.x, self.y, 
                                self.x + X_DIAM, self.y + Y_DIAM, 
                                fill=self.color)

    def move(self):
        """
        Deplacement de la balle

        """
        # nouvelles coordonnes
        self.x = self.x + self.vx
        self.y = self.y + self.vy
        if self.x > (WIDTH - X_DIAM) or self.x < 0:
            self.vx = -self.vx
        if self.y > (HEIGHT - Y_DIAM) or self.y < 0:
            self.vy = -self.vy

        self.balle = self.canvas.create_oval(self.x, self.y, 
                                self.x + X_DIAM, self.y + Y_DIAM, 
                                fill=self.color)

    def efface(self):
        """
        Efface la balle

        """
        self.canvas.delete(self.balle)
        
root = Tk() # init environnement graphique


# Creation du Canvas (fenetre graphique)
can = Canvas(root, width = WIDTH, height = HEIGHT)
can.pack()

colorNames = []

for c in open("rgb.txt").readlines():
    colorNames.append(c.split('\t')[-1].strip('\n'))

nbBalles = 20 # nombre de balles

balles = []
for i in xrange(nbBalles):
    balles.append(Balle(can, randint(X_DIAM, WIDTH - X_DIAM - 1), randint(Y_DIAM, HEIGHT - Y_DIAM - 1),
                        randint(0, 30), randint(0,30), choice(colorNames)))
    
while 1:
    root.update() # force mise a jour du dessin
    time.sleep(DELAY)
    
    for balle in balles:
        balle.efface()
        balle.move()
