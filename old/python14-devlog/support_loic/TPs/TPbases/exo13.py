#!/usr/bin/env python

import numpy
import math
import time

def timer(f, fargs=()):
    t1 = time.clock()
    f(*fargs)
    return time.clock() - t1

def trapeze_boucle(a, b, f, n):
    h = (b - a)/float(n)
    s = 0.
    x = a
    for i in range(1,n,1):
        x += h
        s += f(x)
    s = 0.5*(f(a) + f(b)) + s
    return h*s

def trapeze_vectorized(a, b, f, n):
    h = (b - a)/float(n)
    points = numpy.linspace(a, b, n + 1)
    v = f(points)
    s = sum(v) - 0.5*(v[0] + v[-1])
    return h*s

def f1(x):
    return 1 + 2*x

def fnumpy(x):
    return numpy.sin(x) + x**2

def fmath(x):
    return math.sin(x) + x**2

t1 = timer(trapeze_boucle, (0, 1, f1, 100000))
t2 = timer(trapeze_vectorized, (0, 1, f1, 100000))

print "methode des trapezes pour f(x) = 1 + 2*x"
print "temps d'execution pour boucle Python:", t1 
print "temps d'execution pour Python vectorise:", t2 

t1 = timer(trapeze_boucle, (0, 1, fnumpy, 100000))
t2 = timer(trapeze_vectorized, (0, 1, fnumpy, 100000))
t3 = timer(trapeze_boucle, (0, 1, fmath, 100000))

print "methode des trapezes pour f(x) = sin(x) + x^2"
print "temps d'execution pour boucle Python + numpy.sin:", t1 
print "temps d'execution pour boucle Python + math.sin:", t3 
print "temps d'execution pour Python vectorise:", t2 

