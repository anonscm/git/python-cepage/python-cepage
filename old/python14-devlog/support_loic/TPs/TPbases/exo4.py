#!/usr/bin/env python

A = [1, 2, 3, 4, 5, 6]

B = [0]*len(A)

for i in xrange(len(A)):
    B[i] = A[i]

print B
# verification de la copie
print id(A), id(B)
