#!/usr/bin/env python

import numpy

def condition_initiale(x):
    if numpy.isscalar(x):
        return 3.0
    else:
        return 3.*numpy.ones(x.shape)

y = 1
print condition_initiale(y)
y = numpy.zeros((2,2))
print condition_initiale(y)
