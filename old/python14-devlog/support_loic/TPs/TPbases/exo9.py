#!/usr/bin/env python

import os, re

def cherche(path, exp):
    expreg = re.compile(exp)
    index = 0
    for ligne in open(path).readlines():
        if expreg.search(ligne): print path, index, ligne.replace('\n','')
        index += 1

cherche('test1.txt', 'toto')
