#!/usr/bin/env python

dico_anglais={'computer': 'ordinateur', 'mouse': 'souris',
              'printer': 'imprimante', 'keyboard': 'clavier'}

dico_francais={}

for k, v in dico_anglais.iteritems():
    dico_francais[v] = k

print dico_francais
