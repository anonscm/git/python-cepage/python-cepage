#!/usr/bin/env python
import sys, os

def parcours(path, prof):
    abspath = os.path.join(os.getcwd(), path)
    for f in os.listdir(abspath):
        if os.path.isdir(os.path.join(abspath, f)):
            print '\t'*prof + f + '/'
            parcours(os.path.join(abspath, f), prof + 1)
        else:
            print '\t'*prof + f

parcours('..',0)
