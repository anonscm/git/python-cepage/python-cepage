#!/usr/bin/env python

# globales
code = zip([1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
           ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"])

# fonctions
def decToRoman(num):
    res = []
    for d, r in code:
        while num >= d:
            res.append(r)
            num -= d
    return ''.join(res)

# programme principal

n = int(raw_input('Entrez un entier [1 .. 4000[ : '))

while n < 1 or n > 3999:
    n = int(raw_input('Entrez un entier [1 .. 4000[, s.v.p. : '))

print decToRoman(n)
