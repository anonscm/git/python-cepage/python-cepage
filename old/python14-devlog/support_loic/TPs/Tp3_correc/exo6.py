#!/usr/bin/env python
import sys, os

file = raw_input("Entrez le nom du fichier \n")

if not os.path.exists(os.path.join(os.getcwd(), file)):
    print "le fichier " + file + " n'a pas ete trouve."
    sys.exit()

print "Que voulez-vous faire ?"
print "1: lire le fichier"
print "2: ecrire dans le fichier"

choix = input("Votre choix ? (1 ou 2) \n") 

if choix == 1:
    for l in open(file, 'r'):
        print l

if choix == 2:
    f = open(file, 'a')
    line = -1
    while line != '':
        line = raw_input("Entrez la nouvelle ligne \n")
        if line != '':
            f.write(line + '\n')
    f.close()
