#!/usr/bin/env python
import sys, os, re
from exo8 import cherche

def grep(path, exp):
    abspath = os.path.join(os.getcwd(), path)
    for f in os.listdir(abspath):
        if os.path.isdir(os.path.join(abspath, f)):
            grep(os.path.join(abspath, f), exp)
        else:
            cherche(os.path.join(abspath, f), exp)

# on cherche la chaine exp dans le path
# exemple python exo9.py python ..
# on cherche la chaine 'python' a partir du repertoire d'en dessous
if len(sys.argv) != 3:
    sys.exit( "usage: python exo9.py exp path")

grep(sys.argv[2], sys.argv[1])

