#!/usr/bin/env python

def minMaxMoy(liste):
    "Renvoie le min, le max et la moyenne de la liste"
    if len(liste) == 0:
        return None
    min = max = som = liste[0]

    for i in liste[1:]:
        if i < min:
            min = i
        if i > max:
            max = i
        som = som + i

    return (min, max, som/float(len(liste)))


lp = [10, 18, 14, 20, 12, 16]
print "min : %d, max : %d, moy : %.2f" % (minMaxMoy(lp))
