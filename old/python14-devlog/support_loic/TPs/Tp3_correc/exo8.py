#!/usr/bin/env python
import os, re

def cherche(path, exp):
    expreg = re.compile(exp)
    index = 1
    for ligne in open(path):
        if expreg.search(ligne): 
            print path, ': ligne', index
            print '\t', ligne.replace('\n', '')
        index = index + 1

if __name__ == '__main__':
    cherche('exo9.py', 'os.path')
