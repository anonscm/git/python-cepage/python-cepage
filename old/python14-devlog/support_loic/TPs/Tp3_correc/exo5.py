#!/usr/bin/env python
import sys, os

def my_cp(source, dest):

    fd = open(dest, 'w')

    for ligne in open(source, 'r'):
        fd.write(ligne)

    fd.close()

if len(sys.argv) != 3:
    sys.exit( "usage: python exo1.py filename filedest")

rep=''

#test si le fichier existe
if os.path.exists(os.path.join(os.getcwd(), sys.argv[1])):
    if os.path.exists(os.path.join(os.getcwd(), sys.argv[2])):
        while rep not in ['non', 'oui']:
            rep = raw_input('le fichier ' + sys.argv[2] + " existe deja, voulez vous l'ecraser ? (oui ou non) \n")
        if rep == 'oui':
            my_cp(sys.argv[1], sys.argv[2])
    else:
        my_cp(sys.argv[1], sys.argv[2])
else:
    print 'le fichier ' + sys.argv[1] + " n'existe pas, impossible de le copier."
