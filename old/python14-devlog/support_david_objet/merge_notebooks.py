
import json
import os

folder = "."
prefix = "PythonDevlogObjets"
files = sorted(os.listdir(folder))
paths = [os.path.join(folder, name) for name in files if name.startswith(prefix) and name.endswith(".ipynb")]
result = json.loads(open(paths.pop(0), "r").read())
for path in paths:
    result["worksheets"][0]["cells"].extend(json.loads(open(path, "r").read())["worksheets"][0]["cells"])
open(os.path.join(folder, "%s.ipynb" % prefix), "w").write(json.dumps(result, indent = 1))

