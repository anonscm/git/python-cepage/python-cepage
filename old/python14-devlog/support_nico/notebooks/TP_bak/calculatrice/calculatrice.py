# -*- coding: utf-8 -*-
import sys
import parser

#from operateurs.addition import addition
#from operateurs.soustraction import soustraction
#from operateurs.multiplication import multiplication
#from operateurs.division import division


#lis le __init__ du package mais ne charge pas les modules dans l'espace de noms
import operateurs
op = operateurs.operations
from operateurs import * #charge les modules du package dans l'espace de nom comme attribut du package

numList = []
opList = []

def nombre_op():
    """
    retourne le nombre d'opérateur dans l'expression à calculer.
    """
    return len(opList) - 1


def calcul():
    """
    calcul du résultat de l'expression
    parcours les listes d'operateur et de nombre et effectue le calcul
    zip(*iterables) Make an iterator that aggregates elements from each of the iterables.
    """
    res = 0
    for o, num in zip(opList, numList):
        """
        #avec un init vide dans operateur
        if o == "+" :
            res = addition(res,num)
        if o == "-" :
            res = soustraction(res,num)
        if o == "*" :
            res = multiplication(res,num)
        if o == "/":
            res = division(res,num)
        """
        module = getattr(operateurs,op[o])
        res = getattr(module,op[o])(res, num)
    return res


if __name__ == '__main__':
    expression = "%s" % sys.argv[1]
    print expression
    numList, opList = parser.parser(expression)
    print "%s %s" % (numList, opList)
    print "nombre d'opérateur : %s" %nombre_op()
    resultat = calcul()
    print "resultat : %s" % resultat
