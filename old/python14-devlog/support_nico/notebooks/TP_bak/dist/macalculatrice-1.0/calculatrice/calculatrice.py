# -*- coding: utf-8 -*-
import sys
import parser

from operateurs.addition import addition
from operateurs.soustraction import soustraction
from operateurs.multiplication import multiplication
from operateurs.division import division

numList = []
opList = []

def nombre_op():
    """
    retourne le nombre d'opérateur dans l'expression à calculer.
    """
    return len(opList) - 1


def calcul():
    """
    calcul du résultat de l'expression
    parcours les listes d'operateur et de nombre et effectue le calcul
    zip(*iterables) Make an iterator that aggregates elements from each of the iterables.
    """
    res = 0
    for o, num in zip(opList, numList):
        if o == "+" :
            res = addition(res,num)
        if o == "-" :
            res = soustraction(res,num)
        if o == "*" :
            res = multiplication(res,num)
        if o == "/":
            res = division(res,num)
    return res


if __name__ == '__main__':
    expression = "%s" % sys.argv[1]
    print expression
    numList, opList = parser.parser(expression)
    print "%s %s" % (numList, opList)
    print "nombre d'opérateur : %s" %nombre_op()
    resultat = calcul()
    print "resultat : %s" % resultat
