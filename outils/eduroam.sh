#/bin/bash

if [ "$(id -un)" != "root" ]; then
    sudo $0
    rc=$?
    if [ $rc -ne 0 ]; then
	echo "ERREUR... bye"
	exit $rc
    fi
    epiphany-browser "http://cepage.aquitaine.cnrs.fr/doku.php"
    exit $?
fi

DRIVER="wext"
IFACE="wlan0"
SSID="eduroam"
killall wpa_supplicant
killall dhclient
echo "connexion au reseau : $SSID"
stty -echo
printf "identifiant pour $SSID : "
read ID
stty echo
stty -echo
printf "\n"
printf "mot de passe pour $ID : "
read pass
sudo stty echo
printf "\n"
wpa_supplicant -i $IFACE -c /etc/wpa_supplicant/wpa_supplicant.conf -D $DRIVER -B
wpa_cli identity $SSID $ID
wpa_cli password $SSID $pass
wpa_cli password logon
dhclient $IFACE
